#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
using namespace std;


int main()
{
    const int b = 6;
    const int c = 4;
    int a[b][c] = { {2, 4}, {1, 3} };
    for (int i = 0; i < b; i++)
    {
        for (int j = 0; j < c; j++)
        {
            a[i][j] = i + j;
            cout << a[i][j] << '/n';
        }
        cout << endl;
    }
    cout << endl;
    
    int sum = 0;
    time_t t;
    time(&t);
    int day = localtime(&t)->tm_mday;

    for (int x = 0; x < c; x++)
    {
        sum += a[day % c][x];
    }
    cout << sum << endl;

    return 0;
   
}